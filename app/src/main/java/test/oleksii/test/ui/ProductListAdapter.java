package test.oleksii.test.ui;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import test.oleksii.test.R;
import test.oleksii.test.model.Product;

public class ProductListAdapter extends PagedListAdapter<Product, ProductListAdapter.ViewHolder> {

    protected ProductListAdapter(@NonNull DiffUtil.ItemCallback<Product> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public ProductListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_item, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListAdapter.ViewHolder holder, int position) {
        Product product = getItem(position);
        if (product != null) {
            holder.bind(product);
        } else {
            holder.clear();
        }

    }

    class ViewHolder extends RecyclerView.ViewHolder{
        AppCompatImageView ivProductImage;
        TextView tvName;
        TextView tvDescription;
        TextView tvReference;
        TextView tvPrice;

        ViewHolder(View itemView) {
            super(itemView);
            ivProductImage = itemView.findViewById(R.id.ivImage);
            tvName = itemView.findViewById(R.id.tvName);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvReference = itemView.findViewById(R.id.tvReference);
            tvPrice = itemView.findViewById(R.id.tvPrice);
        }


        void bind(Product product) {
            Picasso.get()
                    .load(product.getIdDefaultImage())
                    .placeholder(R.drawable.product_img_placeholder)
                    .into(ivProductImage);
            tvName.setText(product.getName());
            tvDescription.setText(product.getDescription());
            tvReference.setText(product.getReference());
            tvPrice.setText(String.format(Locale.getDefault(), "%.2f ₴", product.getPrice()));
        }

        void clear() {
           ivProductImage.setImageURI(null);
            tvName.setText(null);
            tvDescription.setText(null);
            tvReference.setText(null);
            tvPrice.setText(null);
        }
    }
}
