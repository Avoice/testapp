package test.oleksii.test.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import test.oleksii.test.ProductListViewModel;
import test.oleksii.test.R;
import test.oleksii.test.model.Product;

public class MainActivity extends AppCompatActivity {
    //UI
    private TextView tvWarning;
    private SwipeRefreshLayout swipeRefresh;

    private ProductListAdapter productListAdapter = new ProductListAdapter(Product.DIFF_CALLBACK);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ProductListViewModel productListViewModel = ViewModelProviders.of(this)
                .get(ProductListViewModel.class);
        initUi();
        subscribeUi(productListViewModel);
    }

    private void initUi() {
        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //List
        RecyclerView rvProductList = findViewById(R.id.rvProductList);
        rvProductList.setLayoutManager(new LinearLayoutManager(this,
                LinearLayout.VERTICAL,
                false));
        rvProductList.setAdapter(productListAdapter);

        //Warning view
        tvWarning = findViewById(R.id.tvWarn);
        //SwipeRefresh
        swipeRefresh = findViewById(R.id.swipeRefresh);
    }

    private void subscribeUi(ProductListViewModel productListViewModel) {
        //List
        subscribeList(productListViewModel, false);
        swipeRefresh.setOnRefreshListener(() -> subscribeList(productListViewModel, true));

        //ProgressBar
        productListViewModel.dataLoadStatus().observe(this, status -> {
            if (status == null) {
                return;
            }
            switch (status) {
                case LOADING:
                    swipeRefresh.setRefreshing(true);
                    break;
                case LOADED:
                    swipeRefresh.setRefreshing(false);
                    break;
                case FAILED:
                    swipeRefresh.setRefreshing(false);
                    //Show error
                    break;
            }
        });
    }

    private void subscribeList(ProductListViewModel productListViewModel, boolean clearList) {
        if(clearList) {
            productListViewModel.clearList();
            swipeRefresh.setRefreshing(false); //"progress bar" will be invoked separately by dataload status observer
        }
        productListViewModel.getProducts().observe(this, pagedList -> {
            if (pagedList != null && pagedList.size() > 0) {
                tvWarning.setVisibility(View.GONE);
            } else {
                tvWarning.setVisibility(View.VISIBLE);
            }
            productListAdapter.submitList(pagedList);
        });
    }
}
