package test.oleksii.test.model;

import android.support.v7.util.DiffUtil;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;



@Root(name = "product", strict = false)
public class Product {
    @Element(name = "id_default_image", required = false, type=ImageLink.class)
    private ImageLink idDefaultImage;

    @Element(name = "reference", required = false)
    private String reference;

    @Element(name = "price", required = false)
    private float price;

    @Path("name/language[1]")
    @Text(required = false)
    private String name;

    @Path("description/language[1]")
    @Text(required = false)
    private String description;

    public String getIdDefaultImage() {
        return idDefaultImage.getLink();
    }


    public String getReference() {
        return reference;
    }

    public float getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return removeTags(description);
    }

    /**
     * Removes <p> tags from text
     * @param description
     * @return
     */
    private String removeTags(String description) {
        return description != null ? android.text.Html.fromHtml(description).toString() : description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    @Root()
    private static class ImageLink {
        public ImageLink() {
        }

        @Namespace(reference = "http://www.w3.org/1999/xlink", prefix = "xlink")
        @Attribute(name = "href", required = false)
        String link;

        @Text(required = false)
        private String text;

        public String getLink() {
            return link;
        }
    }

    public static DiffUtil.ItemCallback<Product> DIFF_CALLBACK = new DiffUtil.ItemCallback<Product>() {
        @Override
        public boolean areItemsTheSame(Product oldItem, Product newItem) {
            return oldItem.name.equals(newItem.name);
        }

        @Override
        public boolean areContentsTheSame(Product oldItem, Product newItem) {
            return oldItem.equals(newItem);
        }
    };
}
