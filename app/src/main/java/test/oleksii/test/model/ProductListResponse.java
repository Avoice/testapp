package test.oleksii.test.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(name = "prestashop", strict = false)
public class ProductListResponse {
    @ElementList(entry="apple", type=Product.class)
    private List<Product> products;

    public List<Product> getProductList() {
        return products == null ? new ArrayList<>() : products;
    }

}
