package test.oleksii.test;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;
import android.support.annotation.NonNull;

import test.oleksii.test.model.Product;
import test.oleksii.test.repository.DataLoadState;
import test.oleksii.test.repository.ProductRepository;

public class ProductListViewModel extends AndroidViewModel {
    private ProductRepository repository;

    public ProductListViewModel(@NonNull Application application) {
        super(application);
        repository = new ProductRepository();
    }

    public LiveData<PagedList<Product>> getProducts() {
        return repository.getProducts();
    }

    public LiveData<DataLoadState> dataLoadStatus() {
        return repository.getDataLoadStatus();
    }

    public void clearList() {
        repository.clearList();
    }
}
