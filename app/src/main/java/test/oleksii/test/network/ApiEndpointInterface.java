package test.oleksii.test.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import test.oleksii.test.model.ProductListResponse;

public interface ApiEndpointInterface {
    @GET("products")
    Call<ProductListResponse> getProducts(@Query("display") String displayParameters, @Query("limit") String limit );
}
