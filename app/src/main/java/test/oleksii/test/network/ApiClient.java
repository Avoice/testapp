package test.oleksii.test.network;

import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class ApiClient {
    public static final String API_BASE_URL =
            "http://ps1722.weeteam.net/api/";
    private static OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
    private static OkHttpClient okHttpClient;
    private static Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(SimpleXmlConverterFactory.createNonStrict(
                            new Persister(new AnnotationStrategy())));
    private static Retrofit retrofitClient = null; //Server api client

    public static OkHttpClient httpClient(String username, String password) {
        if (okHttpClient == null) {
            String authToken = Credentials.basic(username, password);
            AuthenticationInterceptor authenticationInterceptor =
                    new AuthenticationInterceptor(authToken);

            httpClientBuilder.addInterceptor(authenticationInterceptor);
            /*HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClientBuilder.addInterceptor(loggingInterceptor);*/
            okHttpClient = httpClientBuilder.build();
        }
        return okHttpClient;
    }

    public static ApiEndpointInterface getService(String username, String password) {
        if(retrofitClient == null) {
            retrofitClient = builder.client(httpClient(username, password)).build();
        }
        return retrofitClient.create(ApiEndpointInterface.class);
    }

}
