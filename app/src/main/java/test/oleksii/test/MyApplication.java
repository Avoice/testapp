package test.oleksii.test;

import android.app.Application;

import com.squareup.picasso.Downloader;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import test.oleksii.test.network.ApiClient;

public class MyApplication extends Application {
    private Picasso picassoClient;
    public Picasso getPicasso(String username, String password) {
        if (picassoClient == null) {
            Downloader downloader = new OkHttp3Downloader(ApiClient.httpClient(username, password));

            picassoClient = new Picasso.Builder(this)
                    .downloader(downloader)
                    .build();
        }
        return picassoClient;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Picasso.setSingletonInstance(getPicasso(Constants.USERNAME, Constants.PASSWORD));
    }
}
