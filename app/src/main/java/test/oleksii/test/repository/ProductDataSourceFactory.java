package test.oleksii.test.repository;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import test.oleksii.test.Constants;
import test.oleksii.test.model.Product;
import test.oleksii.test.network.ApiClient;

public class ProductDataSourceFactory extends DataSource.Factory<Long, Product> {

    public MutableLiveData<ProductsDataSource> datasourceLiveData = new MutableLiveData<>();
    @Override
    public DataSource<Long, Product> create() {
        ProductsDataSource dataSource = new ProductsDataSource(ApiClient
                .getService(Constants.USERNAME, Constants.PASSWORD));
        datasourceLiveData.postValue(dataSource);
        return dataSource;
    }
}
