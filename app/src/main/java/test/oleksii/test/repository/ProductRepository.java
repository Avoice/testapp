package test.oleksii.test.repository;

import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.support.annotation.MainThread;

import test.oleksii.test.Constants;
import test.oleksii.test.model.Product;

import static android.arch.lifecycle.Transformations.switchMap;

public class ProductRepository {
    private ProductDataSourceFactory dataSourceFactory;
    private LiveData<PagedList<Product>> products;

    public ProductRepository() {
        initList();
    }

    private void initList() {
        dataSourceFactory = new ProductDataSourceFactory();
        PagedList.Config config = new PagedList.Config.Builder()
                .setInitialLoadSizeHint(Constants.PAGE_SIZE)
                .setPageSize(Constants.PAGE_SIZE)
                .setPrefetchDistance(5)
                .setEnablePlaceholders(false)
                .build();

        products = new LivePagedListBuilder<Long,Product>(dataSourceFactory, config)
                .build();
    }

    @MainThread
    public LiveData<PagedList<Product>> getProducts() {
        return products;
    }

    public void clearList() {
        initList();
    }

    public LiveData<DataLoadState> getDataLoadStatus(){
        return switchMap(dataSourceFactory.datasourceLiveData,
                dataSource -> dataSource.loadState);
    }
}
