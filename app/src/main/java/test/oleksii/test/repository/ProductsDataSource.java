package test.oleksii.test.repository;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import test.oleksii.test.model.Product;
import test.oleksii.test.model.ProductListResponse;
import test.oleksii.test.network.ApiEndpointInterface;

public class ProductsDataSource extends PageKeyedDataSource<Long, Product> {
    private ApiEndpointInterface apiClient;
    public MutableLiveData<DataLoadState> loadState;

    public ProductsDataSource(ApiEndpointInterface apiClient) {
        this.apiClient = apiClient;
        loadState = new MutableLiveData<>();
    }



    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<Long, Product> callback) {
        Call<ProductListResponse> requets = apiClient.getProducts("[name,description,id_default_image,price,reference]",
                0 + "," + params.requestedLoadSize);
        Response<ProductListResponse> response = null;
        loadState.postValue(DataLoadState.LOADING);
        try {
            response = requets.execute();
            if (response != null) {
                callback.onResult(response.body().getProductList(),(long)0, (long)1);
            } else {
                callback.onResult(null, null, (long)1);
            }
            loadState.postValue(DataLoadState.LOADED);
        } catch (IOException e) {
            loadState.postValue(DataLoadState.FAILED);
            e.printStackTrace();
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, Product> callback) {
// ignored, since we only ever append to our initial load
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, Product> callback) {
        long fromId = params.key * params.requestedLoadSize - 1;
        Call<ProductListResponse> requets = apiClient.getProducts("[name,description,id_default_image,price,reference]",
                fromId  + "," + params.requestedLoadSize);
        Response<ProductListResponse> response = null;

        loadState.postValue(DataLoadState.LOADING);
        try {
            response = requets.execute();
            if (response != null) {
                callback.onResult(response.body().getProductList(), params.key + 1);
            } else {
                callback.onResult(null, params.key - 1);
            }
            loadState.postValue(DataLoadState.LOADED);
        } catch (IOException e) {
            loadState.postValue(DataLoadState.FAILED);
            e.printStackTrace();
        }
    }
}
