package test.oleksii.test.repository;


public enum DataLoadState {

    LOADING,
    LOADED,
    FAILED

}